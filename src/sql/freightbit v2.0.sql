CREATE DATABASE `freightbit` /*!40100 DEFAULT CHARACTER SET utf8 */;
CREATE TABLE `client` (
  `clientId` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(10) NOT NULL,
  `descripton` varchar(255) DEFAULT NULL,
  `status` varchar(8) NOT NULL,
  PRIMARY KEY (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `group` (
  `clientId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `creationDate` datetime NOT NULL,
  `modifiedDate` datetime NOT NULL,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `name` (`name`),
  KEY `fk.clientId_idx` (`clientId`),
  CONSTRAINT `fk.group_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`clientId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `group_user` (
  `clientId` bigint(20) NOT NULL,
  `groupUserId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  PRIMARY KEY (`groupUserId`),
  KEY `fk_groupuser_group_idx` (`groupId`),
  KEY `fk.groupuser_user_idx` (`userId`),
  KEY `fk.groupuser_client_idx` (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `parameter` (
  `parameter_key` varchar(45) NOT NULL,
  `parameter_value` varchar(45) NOT NULL,
  `table` varchar(45) DEFAULT NULL,
  `clientId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`parameter_key`,`parameter_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `permission` (
  `clientId` bigint(20) NOT NULL,
  `permissionId` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `creationDate` datetime NOT NULL,
  `modifiedDate` datetime NOT NULL,
  PRIMARY KEY (`permissionId`),
  UNIQUE KEY `name` (`name`),
  KEY `fk.permission_client_idx` (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `permission_user_group` (
  `clientId` bigint(20) NOT NULL,
  `permissionId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `projectId` bigint(20) DEFAULT NULL,
  `value` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`permissionId`),
  KEY `fk.permissionusergroup_user_idx` (`userId`),
  KEY `fk.permissionusergroup_client_idx` (`clientId`),
  KEY `fk.permissionusergroup_group_idx` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
  `clientId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `creationDate` datetime NOT NULL,
  `modifiedDate` datetime NOT NULL,
  `lastVisitDate` datetime DEFAULT NULL,
  `status` varchar(8) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `username` (`username`),
  KEY `clientId_idx` (`clientId`),
  CONSTRAINT `fk.user_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`clientId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SELECT * FROM freightbit.user;